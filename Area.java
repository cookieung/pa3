
/**
 * It's a store of Area of unit for convertion.
 * @author Salilthip 5710546640
 *
 */
public enum Area implements Unit{
	
	/*  Define the members of the enumeration
	   *  The attributes are:
	   *  name = a string name for this unit, 
	   *  value = multiplier to convert to square meters.
	   */
	SQUARE_METER( "Square Meter", Math.pow(Length.METER.getValue(),2) ),
	SQUARE_FOOT( "Square Foot", Math.pow(Length.FOOT.getValue(),2) ),
	SQUARE_MILE( "Square Mile",Math.pow(Length.MILE.getValue(),2)),
	SQUARE_INCH( "Square Inch", Math.pow(Length.INCH.getValue(),2)),
	SQUARE_YARD( "Square Yard", Math.pow(Length.YARD.getValue(),2) ),
	SQUARE_MICRON( "Square Micron", Math.pow(Length.MICRON.getValue(),2) ),
	SQUARE_WA( "Square Wa", Math.pow(Length.WA.getValue(),2) ),
	SQUARE_CENTIMETER("Square Centimeter",Math.pow(Length.CENTIMETER.getValue(),2)),
	SQUARE_KILOMETER( "Square Kilometer", Math.pow(Length.KILOMETER.getValue(),2)); 


		
	/**
	 *  name of this unit 
	 *  
	 */
	public final String name;
	
	/**
	 *  It's a multiplier to convert this unit to square meters.
	 */
	public final double value;
		
	/**
	 * Constructor for members of the enum.
	 */
	Area(String name, double value){
		this.name=name;
		this.value=value;
	}
	
	/**
	 * Get value of Area.
	 */
	public double getValue() { return value; }
	
	/**
	 * Get detail Area.
	 */
	public String toString() { return name; }
		
	@Override
	/**
	 * It's a conversion method.
	 */
	public double convertTo(double amt, Unit unit) {
		amt *= this.value;
		double keep =unit.getValue();
		return amt/keep;
	}


}
