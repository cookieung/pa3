
/**
 * It's a store of all types.
 * @author Salilthip 5710546640
 *
 */
public enum UnitType {
	
	/*  Define the members of the enumeration
	   */
	LENGTH(Length.values()),
	AREA(Area.values()),
	WEIGHT(Weight.values()),
	VOLUME(Volume.values());
	
	/**
	 * Array for store the units.
	 */
	public Unit[] unit;
	
	/**
	 * For keep each type.
	 * @param unit is the enum that will keep.
	 */
	private UnitType(Unit[] unit){this.unit=unit;}
	
	/**
	 * For get the array of unit of each type.
	 * @return array of unit
	 */
	public Unit[] getUnitType(){
		return unit;
	}

}