

/**
 * The process that convert the unit to be a require value.
 * @author Salilthip 5710546640
 *
 */
public class UnitConverter{
	
/**
 * For convert the unit.
 * @param amount is the original value.
 * @param fromUnit is the original unit.
 * @param toUnit is the require unit.
 * @return the value of require unit.
 */
	public double convert(double amount, Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	
	/**
	 * For get all of unit of the unit type.
	 * @param unit is the type that want to know.
	 * @return the array of this type.
	 */
	public Unit[] getUnits(UnitType unit){
		return UnitType.valueOf(unit.name()).getUnitType();
	}
	
}
