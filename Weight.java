
/**
 * It's a store of weight of unit for convertion.
 * @author Salilthip 5710546640
 *
 */
public enum Weight implements Unit{
	
	/*  Define the members of the enumeration
	   *  The attributes are:
	   *  name = a string name for this unit, 
	   *  value = multiplier to convert to Weight.
	   */
	GRAM( "Gram", 1.0 ),
	CENTIGRAM( "Centigram", 0.01 ),
	KILOGRAM( "Kilogram",1000.0),
	POUND( "Pound",  453.59237),
	SALUENG( "Salueng", 3.75),
	OUNCE( "Ounce", 28.349523125 );


		
	/**
	 *  name of this unit 
	 *  
	 */
	public final String name;
	
	/**
	 *  It's a multiplier to convert this unit to gram.
	 */
	public final double value;
		
	/**
	 * Constructor for members of the enum.
	 */
	Weight(String name, double value){
		this.name=name;
		this.value=value;
	}
	
	/**
	 * Get value of Weight.
	 */
	public double getValue() { return value; }
	
	/**
	 * Get detail of Weight.
	 */
	public String toString() { return name; }
		
	@Override
	/**
	 * It's a conversion method.
	 */
	public double convertTo(double amt, Unit unit) {
		amt *= this.value;
		double keep =unit.getValue();
		return amt/keep;
	}


}
