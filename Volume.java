
/**
 * It's a store of Volume of unit for convertion.
 * @author Salilthip 5710546640
 *
 */
public enum Volume implements Unit{
	
	/*  Define the members of the enumeration
	   *  The attributes are:
	   *  name = a string name for this unit, 
	   *  value = multiplier to convert to Volume.
	   */
	CUBICMETER("Cubic Meter", 1.0),
	CUBICCENTIMETER("Cubic ", 0.000001),
	LITRE("Litre" , 0.001),
	GALLON("Gallon" , 0.00454609),
	BARREL("Barrel" , 0.158987294928),
	THANK( "Yard", 0.020 );

	
	/**
	 *  name of this unit 
	 *  
	 */
	public final String name;
	
	/**
	 *  It's a multiplier to convert this unit to cubic meters.
	 */
	public final double value;
		
	/**
	 * Constructor for members of the enum.
	 */
	Volume(String name, double value){
		this.name=name;
		this.value=value;
	}
	
	/**
	 * Get value of Volume.
	 */
	public double getValue() { return value; }
	
	/**
	 * Get detail of Volume.
	 */
	public String toString() { return name; }
		
	@Override
	/**
	 * It's a conversion method.
	 */
	public double convertTo(double amt, Unit unit) {
		amt *= this.value;
		double keep =unit.getValue();
		return amt/keep;
	}

}
