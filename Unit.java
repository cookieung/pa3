
/**
 * It's an interface of unit.
 * @author Salilthip 5710546640
 *
 */
public interface Unit {

	/**
	 * 
	 * @param amt
	 * @param unit
	 * @return
	 */
	public double convertTo(double amt, Unit unit);
	
	/**
	 * 
	 * @return
	 */
	public double getValue();
	
	/**
	 * 
	 * @return
	 */
	public String toString();
	
}
