
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * It's an user interface for converter.
 * @author Salilthip 5710546640
 *
 */
public class ConverterUI extends JFrame {

	private UnitConverter unitconverter;
	private String s1,s2;
	private Unit unit1, unit2;
	private Container contents;
	private LayoutManager layout;
	private JButton convertButton, clearButton;
	private JTextField inputValue, resultField;
	private JComboBox<Unit> before;
	private JComboBox<Unit> after;
	private JLabel equal;
	
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuLength;
	private JMenuItem menuArea;
	private JMenuItem menuWeight;
	private JMenuItem menuVolume;
	private JMenuItem menuExit;
	

	/**
	 * Construct a frame for run application.
	 * @param uc is a class for convert the unit.
	 */
	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();
	}

	/**
	 * Initialize components in the window.
	 */
	private void initComponents() {

		before = new JComboBox<Unit>(Length.values());
		after = new JComboBox<Unit>(Length.values());
		contents = new Container();
		layout = new GridLayout(1,6);
		contents.setLayout(layout);
		resultField = new JTextField(10);
		inputValue = new JTextField(10);
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		equal = new JLabel("=");
		
		menubar = new JMenuBar();
		setJMenuBar(menubar);
		menu = new JMenu("Unit Type");
		menuArea = new JMenuItem("Area");
		menuLength = new JMenuItem("Length");
		menuWeight = new JMenuItem("Weight");
		menuVolume = new JMenuItem("Volume");
		menuExit = new JMenuItem("Exit");
		menu.add(menuArea);
		menu.add(menuLength);
		menu.add(menuWeight);
		menu.add(menuVolume);
		menu.addSeparator();
		menu.add(menuExit);
		menubar.add(menu);
		
		contents.add(inputValue);
		contents.add(before);
		equal.setHorizontalAlignment(JLabel.CENTER);
		contents.add(equal);
		contents.add(resultField);
		contents.add(after);
		contents.add(convertButton);
		contents.add(clearButton);

		this.add(contents);
		ActionListener listenerConvert = new ConvertButtonListener();
		convertButton.addActionListener(listenerConvert);
		ActionListener listenerClear = new ClearButtonListener();
		ActionListener exitButton = new ExitAction();
		clearButton.addActionListener(listenerClear);
		inputValue.addActionListener(listenerConvert);
		resultField.addActionListener(listenerConvert);
		menuExit.addActionListener(exitButton);
		ActionListener listenLenght = new selectLength();
		ActionListener listenWeight = new selectWeight();
		ActionListener listenVolume = new selectVolume();
		ActionListener listenArea = new selectArea();
		menuExit.addActionListener(exitButton);
		menuArea.addActionListener(listenArea);
		menuLength.addActionListener(listenLenght);
		menuWeight.addActionListener(listenWeight);
		menuVolume.addActionListener(listenVolume);
	}

	/**
	 * It's a controller of this application on the convertButton.
	 *
	 */
	public class ConvertButtonListener implements ActionListener {
		/**
		 *  method to perform action when user pressed the button or enter.
		 *  User is able to convert in either direction: left-to-right or right-to-left.
		 */
		public void actionPerformed(ActionEvent evt) {
			s1 = inputValue.getText();
			s2 = resultField.getText();
			unit1 = (Unit) before.getSelectedItem();
			unit2 = (Unit) after.getSelectedItem();
			try {
				if (s1.length() > 0) {
					double value = Double.valueOf(s1);
					double result = unitconverter.convert(value, unit1, unit2);
					resultField.setText(String.format("%.8f", result));
				}
				else if(s2.length() > 0){
					double value = Double.valueOf(s2);
					double result = unitconverter.convert(value, unit2, unit1);
					inputValue.setText(String.format("%.8f", result));
				}
				else{
					JOptionPane.showMessageDialog(convertButton, "You must input number on this field");
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(convertButton, "You must input number on this field");
			}

		}
	}

	/**
	 * It's a controller of this application on the clearButton.
	 *
	 */
	public class ClearButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			inputValue.setText("");
			resultField.setText("");
		}
	}
	
	/**
	 * It's a controller of this application on the Exit this application.
	 *
	 */
	class ExitAction implements ActionListener {
		
		public void actionPerformed(ActionEvent evt) {
		System.exit(EXIT_ON_CLOSE);
		}
		}
	
	/**
	 * It will change the list of unit by the menuList.
	 * Change to be Length unit.
	 *
	 */
	class selectLength implements ActionListener {
		
		public void actionPerformed(ActionEvent evt) {
			Unit [] units = unitconverter.getUnits( UnitType.LENGTH );	
			before.removeAllItems();
			after.removeAllItems();
			for( Unit u : units ) before.addItem(u );
			for( Unit u : units ) after.addItem(u );
		}
		}
	
	/**
	 * It will change the list of unit by the menuList.
	 * Change to be Weight unit.
	 *
	 */
	class selectWeight implements ActionListener {
		
		public void actionPerformed(ActionEvent evt) {
			Unit [] units = unitconverter.getUnits( UnitType.WEIGHT );	
			before.removeAllItems();
			after.removeAllItems();
			for( Unit u : units ) before.addItem(u );
			for( Unit u : units ) after.addItem(u );
		}
		}
	
	/**
	 * It will change the list of unit by the menuList.
	 * Change to be Area unit.
	 *
	 */
	class selectArea implements ActionListener {
		
		public void actionPerformed(ActionEvent evt) {
			Unit [] units = unitconverter.getUnits( UnitType.AREA );	
			before.removeAllItems();
			after.removeAllItems();
			for( Unit u : units ) before.addItem( u );
			for( Unit u : units ) after.addItem(u );
		}
		}
	
	/**
	 * It will change the list of unit by the menuList.
	 * Change to be Volume unit.
	 *
	 */
	class selectVolume implements ActionListener {
		
		public void actionPerformed(ActionEvent evt) {
			Unit [] units = unitconverter.getUnits( UnitType.VOLUME );	
			before.removeAllItems();
			after.removeAllItems();
			for( Unit u : units ) before.addItem( u );
			for( Unit u : units ) after.addItem( u );
		}
		}

	/**
	 * Show this application.
	 */
	public void run() {
		this.setVisible(true);
	}


}
