
/**
 * It's a store of length of unit for convertion.
 * @author Salilthip 5710546640
 *
 */
public enum Length implements Unit{
	
	/*  Define the members of the enumeration
	   *  The attributes are:
	   *  name = a string name for this unit, 
	   *  value = multiplier to convert to meters.
	   */
	METER( "Meter", 1.0 ),
	FOOT( "Foot", 0.3048 ),
	MILE( "Mile",1609.344),
	INCH( "Inch",  0.0254),
	YARD( "Yard", 3 ),
	MICRON( "Micron", 1.0E-6 ),
	WA( "Wa", 2 ),
	CENTIMETER("Centimeter",0.01),
	KILOMETER( "Kilometer", 1000.0); 


		
	/**
	 *  name of this unit 
	 *  
	 */
	public final String name;
	
	/**
	 *  It's a multiplier to convert this unit to meters.
	 */
	public final double value;
		
	/**
	 * Constructor for members of the enum.
	 */
	Length(String name, double value){
		this.name=name;
		this.value=value;
	}
	
	/**
	 * Get value of Length.
	 */
	public double getValue() { return value; }
	
	/**
	 * Get detail of Length.
	 */
	public String toString() { return name; }
		
	@Override
	/**
	 * It's a conversion method.
	 */
	public double convertTo(double amt, Unit unit) {
		amt *= this.value;
		double keep =unit.getValue();
		return amt/keep;
	}


}
