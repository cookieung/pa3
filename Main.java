
/**
 * It's an application that converts the require unit.
 * @author Salilthip 5710546640
 *
 */
public class Main {

	/**
	 * It's main of Application.
	 * @param args
	 */
	public static void main(String[] args) {
		ConverterUI a = new ConverterUI(new UnitConverter());
		a.run();
	}
	
}
